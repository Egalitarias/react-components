import React, {ReactElement, useState} from 'react';
import {StyleSheet, View, Modal, ActivityIndicator} from 'react-native';

export const Loader = (): ReactElement => {
  const [isLoading] = useState<boolean>(false);
  const [backgroundColor] = useState<string>('#00000040');
  const [indicatorColor] = useState<string>('#00000040');
  const [size] = useState<string>('small');

  const styles = StyleSheet.create({
    modalBackground: {
      flex: 1,
      alignItems: 'center',
      flexDirection: 'column',
      justifyContent: 'space-around',
    },
    activityIndicatorWrapper: {
      height: 100,
      width: 100,
      borderRadius: 10,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-around',
    },
  });

  return (
    <Modal
      transparent={true}
      animationType={'none'}
      visible={isLoading}
      onRequestClose={() => {
        console.log('close modal');
      }}>
      <View style={[styles.modalBackground, {backgroundColor}]}>
        <View
          style={[
            styles.activityIndicatorWrapper,
            {backgroundColor: indicatorColor},
          ]}>
          <ActivityIndicator animating={isLoading} size={size} />
        </View>
      </View>
    </Modal>
  );
};
