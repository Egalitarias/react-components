import React, {ReactElement} from 'react';
import {View, TextInput, Image, Text} from 'react-native';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {StyleSheet} from 'react-native';

import {colors} from '../style/colors';

type InlineImageTextInputProps = {
  handleChangeText: () => void;
  placeholder: string;
  keyboardType: string;
  icon: string;
  value: number;
  isSecureTextEntry: boolean;
  errorMessage: string;
  isSubDomain: boolean;
  isEditable: boolean;
  pointerEvent: string;
  customStyle: {};
};

export const InlineImageTextInput = ({
  handleChangeText,
  placeholder,
  keyboardType,
  icon,
  value,
  isSecureTextEntry,
  errorMessage,
  isSubDomain,
  isEditable,
  pointerEvent,
  customStyle,
}: InlineImageTextInputProps): ReactElement => {
  const styles = StyleSheet.create({
    container: {
      flexDirection: 'row',
      height: 40,
      borderBottomWidth: 0.5,
      marginTop: 4,
    },
    image: {
      marginTop: 8,
      marginRight: 12,
      height: 20,
      width: 20,
      alignItems: 'center',
    },
    inputBox: {
      flex: 1,
      fontSize: RFPercentage(2.2),
    },
    inputBoxWithError: {
      marginLeft: 12,
      fontSize: RFPercentage(2.2),
      borderBottomColor: colors.RED,
    },
    errorText: {
      marginTop: 1,
      color: colors.RED,
      fontSize: RFPercentage(1.8),
      height: 16,
    },
    bottomBorder: {
      borderBottomColor: colors.RED,
      borderBottomWidth: 1,
    },
    subLabelInputBox: {
      position: 'absolute',
      right: 0,
      alignItems: 'center',
      marginTop: 8,
      fontWeight: 'bold',
      fontSize: RFPercentage(1.8),
      color: colors.SALTBOX_GREY,
    },
  });

  return (
    <View>
      <View
        style={[styles.container, errorMessage ? styles.bottomBorder : null]}>
        {icon ? (
          <Image source={icon} style={styles.image} resizeMode="contain" />
        ) : null}
        <TextInput
          style={[styles.inputBox, customStyle]}
          placeholder={placeholder}
          keyboardType={keyboardType}
          onChangeText={text => handleChangeText(text)}
          value={value}
          secureTextEntry={isSecureTextEntry}
          editable={isEditable}
          pointerEvents={pointerEvent}
        />
        {isSubDomain ? (
          <Text style={[styles.subLabelInputBox, customStyle]}>
            .ensurexper.com
          </Text>
        ) : null}
      </View>
      <Text style={[styles.errorText, customStyle]}>{errorMessage}</Text>
    </View>
  );
};
