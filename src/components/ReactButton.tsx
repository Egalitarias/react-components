import React, { ReactElement } from 'react';
import {TouchableOpacity, Text, StyleSheet, ViewStyle} from 'react-native';
import {RFPercentage} from 'react-native-responsive-fontsize';

export const styles = StyleSheet.create({
  button: {
    height: RFPercentage(5),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 6,
    paddingHorizontal: RFPercentage(2),
    opacity: 1,
  },
  buttonText: {
    fontWeight: 'bold',
    fontSize: RFPercentage(2),
  },
});

type ReactButtonProps = {
  onPress: () => void,
  label: string,
  backgroundColor: string,
  textColor: string,
  disabled: boolean,
  styleButton: ViewStyle,
}
export const ReactButton = ({
  onPress,
  label,
  backgroundColor,
  textColor,
  disabled,
  styleButton,
}: ReactButtonProps): ReactElement => {

    return (
      <TouchableOpacity
        style={[styles.button, {backgroundColor}, styleButton]}
        onPress={() => onPress()}
        disabled={disabled}>
        <Text
          adjustsFontSizeToFit={true}
          style={[styles.buttonText, {color: textColor}]}>
          {label}
        </Text>
      </TouchableOpacity>
    );
  }
}
